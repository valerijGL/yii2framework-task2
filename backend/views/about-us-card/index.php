<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AboutUsCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'About Us Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-us-card-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create About Us Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'status',
            'icon_class',
            'h2',
            'h2_href',
            //'p',
            'smallImage:image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
