<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CallToAction */

$this->title = 'Create Call To Action';
$this->params['breadcrumbs'][] = ['label' => 'Call To Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-to-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
