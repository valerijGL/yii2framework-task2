<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CallToActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Call To Actions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-to-action-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Call To Action', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'status',
            'h2',
            'p',
            'button_href',
            'button_text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
