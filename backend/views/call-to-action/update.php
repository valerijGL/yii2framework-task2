<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CallToAction */

$this->title = 'Update Call To Action: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Call To Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="call-to-action-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
