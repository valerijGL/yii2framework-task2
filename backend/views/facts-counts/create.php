<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FactsCounts */

$this->title = 'Create Facts Counts';
$this->params['breadcrumbs'][] = ['label' => 'Facts Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facts-counts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
