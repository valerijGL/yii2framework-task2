<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FactsCounts */

$this->title = 'Update Facts Counts: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Facts Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facts-counts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
