<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FactsHeader */

$this->title = 'Create Facts Header';
$this->params['breadcrumbs'][] = ['label' => 'Facts Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facts-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
