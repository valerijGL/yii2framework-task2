<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FactsHeader */

$this->title = 'Update Facts Header: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Facts Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facts-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
