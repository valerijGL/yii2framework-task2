<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FeaturedServices */

$this->title = 'Create Featured Services';
$this->params['breadcrumbs'][] = ['label' => 'Featured Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featured-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
