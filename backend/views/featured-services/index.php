<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FeaturedServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Featured Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featured-services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Featured Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'status',
            'icon_class',
            'h2',
            'p',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
