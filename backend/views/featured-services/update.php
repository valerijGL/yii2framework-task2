<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FeaturedServices */

$this->title = 'Update Featured Services: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Featured Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="featured-services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
