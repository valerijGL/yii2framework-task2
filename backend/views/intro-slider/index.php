<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\IntroSliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Добавление слайдера';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intro-slider-index">
<?= Html::encode($this->title) ?>
    <h1>Добавление слайдера</h1>

    <p>
        <?= Html::a('Добавить слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'headline',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
