<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\IntroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="intro-index">

    <h1>Добавление контента на слайдер </h1>

    <p>
        <?= Html::a('Добавить контент', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    Pjax::begin(); ?>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                'status',
                'h2',
                'p',
                'smallImage:image',
                'btn_text',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>

    <?php
    Pjax::end(); ?>

</div>
