<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>VTdebik</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню сайта', 'options' => ['class' => 'header']],


                    ['label' => 'Главная', 'icon' => 'asterisk', 'url' => ['/intro']],
                    ['label' => 'Избранные услуги', 'icon' => 'asterisk', 'url' => ['/featured-services']],
                    [
                        'label' => 'О нас',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/about-us-header']],
                            ['label' => 'Карточки', 'icon' => 'pencil', 'url' => ['/about-us-card']],

                        ]
                    ],
                    [
                        'label' => 'Сервисы',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/services-header']],
                            ['label' => 'Карточки', 'icon' => 'pencil', 'url' => ['/services-cards']],

                        ]
                    ],
                    ['label' => 'Call to action', 'icon' => 'asterisk', 'url' => ['/call-to-action']],
                    [
                        'label' => 'Наши умения',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/our-skills-header']],
                            ['label' => 'Графики', 'icon' => 'pencil', 'url' => ['/our-skills-graphs']],

                        ]
                    ],
                    [
                        'label' => 'Факты',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/facts-header']],
                            ['label' => 'Счетчики', 'icon' => 'pencil', 'url' => ['/facts-counts']],

                        ]
                    ],
                    [
                        'label' => 'Наше портфолио',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/our-portfolio-header']],
                            ['label' => 'Карточки', 'icon' => 'pencil', 'url' => ['/our-portfolio-gallery']],

                        ]
                    ],
                    [
                        'label' => 'Команда',
                        'icon' => 'asterisk',
                        'url' => '#',
                        'items' =>[
                            ['label' => 'Заголовок', 'icon' => 'pencil', 'url' => ['/team-header']],
                            ['label' => 'Карточки', 'icon' => 'pencil', 'url' => ['/team-cards']],

                        ]
                    ],
                    ['label' => 'Форма', 'icon' => 'asterisk', 'url' => ['/contact-form']],
                    ['label' => 'Карта', 'icon' => 'asterisk', 'url' => ['/maps']],
                    ['label' => 'Учетные записи', 'icon' => 'asterisk', 'url' => ['/user/admin']],
//                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
//                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['user/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
