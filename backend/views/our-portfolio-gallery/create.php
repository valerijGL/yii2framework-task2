<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurPortfolioGallery */

$this->title = 'Create Our Portfolio Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Our Portfolio Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-portfolio-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
