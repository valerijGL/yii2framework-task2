<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurPortfolioGallery */

$this->title = 'Update Our Portfolio Gallery: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Our Portfolio Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="our-portfolio-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
