<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurPortfolioHeader */

$this->title = 'Create Our Portfolio Header';
$this->params['breadcrumbs'][] = ['label' => 'Our Portfolio Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-portfolio-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
