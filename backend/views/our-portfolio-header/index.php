<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OurPortfolioHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Our Portfolio Headers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-portfolio-header-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Our Portfolio Header', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'status',
            'h2',
            'p',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
