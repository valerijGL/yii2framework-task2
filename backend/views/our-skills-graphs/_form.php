<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OurSkillsGraphs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-skills-graphs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(['0', '1']) ?>

    <?= $form->field($model, 'h2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percent')->textInput() ?>

    <?= $form->field($model, 'color')->dropDownList(['bg-success'=> 'Зеленый', 'bg-info'=> 'Бирюзовый', 'bg-warning' => 'Желтый', 'bg-danger' => 'Красный']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
