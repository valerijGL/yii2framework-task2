<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurSkillsGraphs */

$this->title = 'Create Our Skills Graphs';
$this->params['breadcrumbs'][] = ['label' => 'Our Skills Graphs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-skills-graphs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
