<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurSkillsGraphs */

$this->title = 'Update Our Skills Graphs: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Our Skills Graphs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="our-skills-graphs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
