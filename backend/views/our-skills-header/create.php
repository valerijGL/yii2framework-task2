<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurSkillsHeader */

$this->title = 'Create Our Skills Header';
$this->params['breadcrumbs'][] = ['label' => 'Our Skills Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-skills-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
