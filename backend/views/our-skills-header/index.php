<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OurSkillsHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Our Skills Headers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-skills-header-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Our Skills Header', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'status',
            'h2',
            'p',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
