<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurSkillsHeader */

$this->title = 'Update Our Skills Header: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Our Skills Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="our-skills-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
