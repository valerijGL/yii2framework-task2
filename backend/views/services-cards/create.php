<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServicesCards */

$this->title = 'Create Services Cards';
$this->params['breadcrumbs'][] = ['label' => 'Services Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-cards-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
