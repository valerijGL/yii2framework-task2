<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServicesHeader */

$this->title = 'Create Services Header';
$this->params['breadcrumbs'][] = ['label' => 'Services Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
