<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TeamCardsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-cards-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'post') ?>

    <?= $form->field($model, 'twitter_href') ?>

    <?php // echo $form->field($model, 'facebook_href') ?>

    <?php // echo $form->field($model, 'google_plus_href') ?>

    <?php // echo $form->field($model, 'linkedin_href') ?>

    <?php // echo $form->field($model, 'image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
