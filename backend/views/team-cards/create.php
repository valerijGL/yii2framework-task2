<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeamCards */

$this->title = 'Create Team Cards';
$this->params['breadcrumbs'][] = ['label' => 'Team Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-cards-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
