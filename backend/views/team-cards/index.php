<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TeamCardsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Team Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-cards-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Team Cards', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'status',
            'name',
            'post',
            'twitter_href',
            'facebook_href',
            'google_plus_href',
            'linkedin_href',
            'smallImage:image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
