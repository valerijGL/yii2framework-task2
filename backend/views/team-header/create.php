<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeamHeader */

$this->title = 'Create Team Header';
$this->params['breadcrumbs'][] = ['label' => 'Team Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
