<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * This is the model class for table "about_us_card".
 *
 * @property int $id
 * @property int $status
 * @property string $icon_class
 * @property string $h2
 * @property string $h2_href
 * @property string $p
 * @property string|null $image
 */
class AboutUsCard extends \yii\db\ActiveRecord
{
    public $file;

    function behaviors()
    {
        return [
            'seo' => [
                'class' => 'dvizh\seo\behaviors\SeoFields',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_us_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'icon_class', 'h2', 'h2_href', 'p'], 'required'],
            [['status'], 'integer'],
            [['icon_class', 'h2_href', 'p', 'image'], 'string', 'max' => 100],
            [['h2'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'icon_class' => 'Icon Class',
            'h2' => 'Заголвок карточки',
            'h2_href' => 'Ссылка заголовка карточки',
            'p' => 'Описание карточки',
            'smallImage' => 'Изображение',
        ];
    }
    public function getSmallImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/';
        return $dir.'100x100/'.$this->image;
    }

    public function getAverageImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/';
        return $dir.'250x250/'.$this->image;
    }

    /**
     * @throws Exception
     */

    public function beforeSave($insert)
    {
        if($file = UploadedFile::getInstance($this, 'file')){
            $dir = Yii::getAlias('@frontend/web'). '/img/';
            if(!empty($this->image)) {
                if (file_exists($dir . $this->image)) {
                    unlink($dir . $this->image);
                }
                if (file_exists($dir . '250x250/' . $this->image)) {
                    unlink($dir . '250x250/' . $this->image);
                }
                if (file_exists($dir . '100x100/' . $this->image)) {
                    unlink($dir . '100x100/' . $this->image);
                }
            }
            $this->image = strtotime('now').'_'.Yii::$app->getSecurity()->generateRandomString(6)  . '.' . $file->extension;
            $file->saveAs($dir.$this->image);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('250','250', Yii\image\drivers\Image::INVERSE);
            $imag->crop('250','250');
            $imag->save($dir.'250x250/'.$this->image, 90);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('100','100', Yii\image\drivers\Image::INVERSE);
            $imag->crop('100','100');
            $imag->save($dir.'100x100/'.$this->image, 90);
        }
        return parent::beforeSave($insert);
    }

}
