<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "call_to_action".
 *
 * @property int $id
 * @property int $status
 * @property string $h2
 * @property string $p
 * @property string|null $button_href
 * @property string|null $button_text
 */
class CallToAction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'call_to_action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'h2', 'p'], 'required'],
            [['status'], 'integer'],
            [['h2'], 'string', 'max' => 35],
            [['p', 'button_href', 'button_text'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'h2' => 'Заголовок',
            'p' => 'Описание',
            'button_href' => 'Ссылка кнопки',
            'button_text' => 'Текст кнопки',
        ];
    }
}
