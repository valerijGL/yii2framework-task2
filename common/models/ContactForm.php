<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "contact_form".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $message
 */
class ContactForm extends \yii\db\ActiveRecord
{
    public $reCaptcha;
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'message'], 'required'],
            [['name'], 'string', 'min' => 2, 'max' => 35],
            [['email', 'subject', 'message'], 'string', 'min' => 10, 'max' => 100],
            [['email'], 'string'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(),
                'secret' => '6LfAMpMbAAAAAFgkWMJByaLUmTut0Te9c72H3L6p', // unnecessary if reСaptcha is already configured
                'uncheckedMessage' => 'Please confirm that you are not a bot.'],
            [['file'], 'file', 'extensions'=> ['pdf'], 'maxSize' => 10000 * 1000]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Your name',
            'email' => 'E-mail',
            'subject' => 'Subject',
            'message' => 'Message',
            'reCaptcha' => '',
            'file' => ''
        ];
    }

}
