<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "facts_counts".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property int $counts
 */
class FactsCounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facts_counts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'title', 'counts'], 'required'],
            [['status', 'counts'], 'integer'],
            [['title'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'title' => 'Название',
            'counts' => 'Счетчик',
        ];
    }
}
