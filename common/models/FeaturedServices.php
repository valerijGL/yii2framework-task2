<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "featured_services".
 *
 * @property int $id
 * @property int $status
 * @property string $icon_class
 * @property string|null $h2
 * @property string|null $p
 */
class FeaturedServices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'featured_services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'icon_class'], 'required'],
            [['status'], 'integer'],
            [['icon_class', 'p'], 'string', 'max' => 100],
            [['h2'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'icon_class' => 'Icon Class',
            'h2' => 'H2',
            'p' => 'P',
        ];
    }
}
