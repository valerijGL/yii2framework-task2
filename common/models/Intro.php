<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "intro".
 *
 * @property int $id
 * @property int $status
 * @property string $h2
 * @property string|null $p
 * @property string $image
 * @property string|null $btn_text Кнопка
 */
class Intro extends ActiveRecord
{
    public $ImageManager_id_avatar;

    function behaviors()
    {
        return [
            'seo' => [
                'class' => 'dvizh\seo\behaviors\SeoFields',
            ],
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'gallery',
                'quality' => 60,
                'galleryId' => 'picture'
            ],
        ];
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'intro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'h2'], 'required'],
            [['status'], 'integer'],
            [['h2'], 'string', 'max' => 35],
            [['p', 'image'], 'string', 'max' => 100],
            [['file'], 'image'],
            [['btn_text'], 'string', 'max' => 20],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'h2' => 'Заголовок',
            'p' => 'Описание',
            'averageImage' => 'Изображение',
            'smallImage' => 'Изображение',
            'btn_text' => 'Текст на кнопке',
        ];
    }

    public function getSmallImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/intro-carousel/';
        return $dir.'100x100/'.$this->image;
    }

    public function getAverageImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/intro-carousel/';
        return $dir.'250x250/'.$this->image;
    }

    /**
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        if($file = UploadedFile::getInstance($this, 'file')){
            $dir = Yii::getAlias('@frontend/web/img'). '/intro-carousel/';
            if(!empty($this->image)) {
                if (file_exists($dir . $this->image)) {
                    unlink($dir . $this->image);
                }
                if (file_exists($dir . '250x250/' . $this->image)) {
                    unlink($dir . '250x250/' . $this->image);
                }
                if (file_exists($dir . '100x100/' . $this->image)) {
                    unlink($dir . '100x100/' . $this->image);
                }
            }
            $this->image = strtotime('now').'_'.Yii::$app->getSecurity()->generateRandomString(6)  . '.' . $file->extension;
            $file->saveAs($dir.$this->image);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('250','250', Yii\image\drivers\Image::INVERSE);
            $imag->crop('250','250');
            $imag->save($dir.'250x250/'.$this->image, 90);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('100','100', Yii\image\drivers\Image::INVERSE);
            $imag->crop('100','100');
            $imag->save($dir.'100x100/'.$this->image, 90);
        }
        return parent::beforeSave($insert);
    }

}
