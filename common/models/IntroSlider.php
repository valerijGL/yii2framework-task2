<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "introslider".
 *
 * @property int $id
 * @property string $headline
 */
class IntroSlider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'introslider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['headline'], 'required'],
            [['headline'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'headline' => 'Название',
        ];
    }
}
