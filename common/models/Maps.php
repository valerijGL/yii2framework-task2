<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "maps".
 *
 * @property int $id
 * @property int|null $status
 * @property string $map_link
 */
class Maps extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['map_link'], 'required'],
            [['map_link'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'map_link' => 'Map Link',
        ];
    }
}
