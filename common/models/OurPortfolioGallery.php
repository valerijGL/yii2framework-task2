<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "our_portfolio_gallery".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $description
 * @property string $image
 */
class OurPortfolioGallery extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_portfolio_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'title'], 'required'],
            [['status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'title' => 'Название',
            'description' => 'Описание',
            'image' => 'Изображение',
        ];
    }
    public function getSmallImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/our-portfolio/';
        return $dir.'100x100/'.$this->image;
    }

    public function getAverageImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/our-portfolio/';
        return $dir.'250x250/'.$this->image;
    }

    /**
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        if($file = UploadedFile::getInstance($this, 'file')){
            $dir = Yii::getAlias('@frontend/web/img'). '/our-portfolio/';
            if(!empty($this->image)) {
                if (file_exists($dir . $this->image)) {
                    unlink($dir . $this->image);
                }
                if (file_exists($dir . '250x250/' . $this->image)) {
                    unlink($dir . '250x250/' . $this->image);
                }
                if (file_exists($dir . '100x100/' . $this->image)) {
                    unlink($dir . '100x100/' . $this->image);
                }
            }
            $this->image = strtotime('now').'_'.Yii::$app->getSecurity()->generateRandomString(6)  . '.' . $file->extension;
            $file->saveAs($dir.$this->image);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('250','250', Yii\image\drivers\Image::INVERSE);
            $imag->crop('250','250');
            $imag->save($dir.'250x250/'.$this->image, 90);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('100','100', Yii\image\drivers\Image::INVERSE);
            $imag->crop('100','100');
            $imag->save($dir.'100x100/'.$this->image, 90);
        }
        return parent::beforeSave($insert);
    }
}
