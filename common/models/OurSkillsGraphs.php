<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "our_skills_graphs".
 *
 * @property int $id
 * @property int $status
 * @property string $h2
 * @property int $percent
 * @property string $color
 */
class OurSkillsGraphs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_skills_graphs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'h2', 'percent', 'color'], 'required'],
            [['status', 'percent'], 'integer'],
            [['h2'], 'string', 'max' => 35],
            [['color'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'h2' => 'Заголовок',
            'percent' => 'Процент',
            'color' => 'Цвет',
        ];
    }
}
