<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "services_cards".
 *
 * @property int $id
 * @property int $status
 * @property string $h2
 * @property string $p
 * @property string|null $icon_class
 */
class ServicesCards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'h2', 'p'], 'required'],
            [['status'], 'integer'],
            [['h2'], 'string', 'max' => 35],
            [['p', 'icon_class'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'h2' => 'Заголовок',
            'p' => 'Описание',
            'icon_class' => 'Класс иконки',
        ];
    }
}
