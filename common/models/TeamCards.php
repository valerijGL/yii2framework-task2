<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "team_cards".
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string $post
 * @property string|null $twitter_href
 * @property string|null $facebook_href
 * @property string|null $google_plus_href
 * @property string|null $linkedin_href
 * @property string|null $image
 */
class TeamCards extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'name', 'post'], 'required'],
            [['status'], 'integer'],
            [['name', 'post', 'twitter_href', 'facebook_href', 'google_plus_href', 'linkedin_href', 'image'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'name' => 'Имя и Фамилия',
            'post' => 'Должность',
            'twitter_href' => 'Ссылка на твиттер',
            'facebook_href' => 'Ссылка на Facebook',
            'google_plus_href' => 'Ссылка на Google Plus',
            'linkedin_href' => 'Ссылка на Linkedin',
            'image' => 'Фото',
        ];
    }
    public function getSmallImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/team/';
        return $dir.'100x100/'.$this->image;
    }

    public function getAverageImage()
    {
        $dir = str_replace('admin', '', Url::home(true)).'frontend/web/img/team/';
        return $dir.'250x250/'.$this->image;
    }

    /**
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        if($file = UploadedFile::getInstance($this, 'file')){
            $dir = Yii::getAlias('@frontend/web/img'). '/team/';
            if(!empty($this->image)) {
                if (file_exists($dir . $this->image)) {
                    unlink($dir . $this->image);
                }
                if (file_exists($dir . '250x250/' . $this->image)) {
                    unlink($dir . '250x250/' . $this->image);
                }
                if (file_exists($dir . '100x100/' . $this->image)) {
                    unlink($dir . '100x100/' . $this->image);
                }
            }
            $this->image = strtotime('now').'_'.Yii::$app->getSecurity()->generateRandomString(6)  . '.' . $file->extension;
            $file->saveAs($dir.$this->image);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('250','250', Yii\image\drivers\Image::INVERSE);
            $imag->crop('250','250');
            $imag->save($dir.'250x250/'.$this->image, 90);
            $imag = Yii::$app->image->load($dir.$this->image);
            $imag->background('#fff',0);
            $imag->resize('100','100', Yii\image\drivers\Image::INVERSE);
            $imag->crop('100','100');
            $imag->save($dir.'100x100/'.$this->image, 90);
        }
        return parent::beforeSave($insert);
    }
}
