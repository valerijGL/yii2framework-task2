<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TeamCards;

/**
 * TeamCardsSearch represents the model behind the search form of `common\models\TeamCards`.
 */
class TeamCardsSearch extends TeamCards
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'post', 'twitter_href', 'facebook_href', 'google_plus_href', 'linkedin_href', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamCards::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'post', $this->post])
            ->andFilterWhere(['like', 'twitter_href', $this->twitter_href])
            ->andFilterWhere(['like', 'facebook_href', $this->facebook_href])
            ->andFilterWhere(['like', 'google_plus_href', $this->google_plus_href])
            ->andFilterWhere(['like', 'linkedin_href', $this->linkedin_href])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
