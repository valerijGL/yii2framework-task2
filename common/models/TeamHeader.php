<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "team_header".
 *
 * @property int $id
 * @property int $status
 * @property string $h2
 * @property string $p
 */
class TeamHeader extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_header';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'h2', 'p'], 'required'],
            [['status'], 'integer'],
            [['h2'], 'string', 'max' => 35],
            [['p'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'h2' => 'Заголовок',
            'p' => 'Описание',
        ];
    }
}
