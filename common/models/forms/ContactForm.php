<?php


namespace common\models\forms;

use Yii;
use yii\base\Model;

/**
 * Description of LoginForm
 *
 * @author admin
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $text;
    public $reCaptcha;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'text'], 'required'],
            [['name'], 'string', 'max' => 35,  'min' => 2],
            [['subject', 'text'], 'string', 'max' => 100, 'min' => 10],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(),
                'secret' => '6LdUrZEbAAAAAMyxCzHhgAeCDoo6ynVGhMu0Z-sR', // unnecessary if reСaptcha is already configured
                'uncheckedMessage' => 'Please confirm that you are not a bot.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Your name',
            'email' => 'E-mail',
            'subject' => 'Subject',
            'text' => 'Message',
            'reCaptcha' => '',
        ];
    }
}