<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;

/**
 * Description of SignupForm
 *
 * @author admin
 */
class SignupForm extends Model
{

    public $username;
    public $email;
    public $password;
    public $id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['username'], 'unique', 'targetClass' => User::className()],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [['email'], 'unique', 'targetClass' => User::className()],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Save user
     * @return User|null
     */
    public function save()
    {
        if ($this->validate()) {
            $user = new User();
            $this->id = $user->id;
            $user->email = $this->email;
            $user->username = $this->username;
            $user->password = $this->password;
            $user->created_at = $time = time();
            $user->updated_at = $time;
            $user->auth_key = Yii::$app->security->generateRandomString();
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);

            if ($user->save()) {
                return $user;
            }
        }
    }
    
}
