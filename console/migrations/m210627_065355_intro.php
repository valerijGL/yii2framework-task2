<?php

use yii\db\Migration;

/**
 * Class m210627_065355_intro
 */
class m210627_065355_intro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('intro', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'h2' => $this->string(35),
            'p' => $this->string(100),
            'image' => $this->string(100),
            'btn-text' => $this->string(20),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('intro');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210627_065355_intro cannot be reverted.\n";

        return false;
    }
    */
}
