<?php

use yii\db\Migration;

/**
 * Class m210709_090411_about_us_card
 */
class m210709_090411_about_us_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('about_us_card', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
            'image' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('about_us_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_090411_about_us_card cannot be reverted.\n";

        return false;
    }
    */
}
