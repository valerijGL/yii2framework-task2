<?php

use yii\db\Migration;

/**
 * Class m210713_082837_contact_form
 */
class m210713_082837_contact_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contact_form', [
            'id' => $this->primaryKey(),
            'name' => $this->string(35)->notNull(),
            'email' => $this->string(100)->notNull(),
            'subject' => $this->string(100)->notNull(),
            'message' => $this->string(100)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contact_form');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210713_082837_contact_form cannot be reverted.\n";

        return false;
    }
    */
}
