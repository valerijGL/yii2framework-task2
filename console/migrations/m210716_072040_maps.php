<?php

use yii\db\Migration;

/**
 * Class m210716_072040_maps
 */
class m210716_072040_maps extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('maps', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'map_link' => $this->string(1000)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('maps');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210716_072040_maps cannot be reverted.\n";

        return false;
    }
    */
}
