<?php

use yii\db\Migration;

/**
 * Class m210906_102715_imageGallery
 */
class m210906_102715_imageGallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('imageGallery', [
            'id' => $this->primaryKey(),
            'alt' => $this->string(100),
            'title' => $this->string(100),
            'description' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('imageGallery');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210906_102715_imageGallery cannot be reverted.\n";

        return false;
    }
    */
}
