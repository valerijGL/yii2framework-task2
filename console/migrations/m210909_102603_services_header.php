<?php

use yii\db\Migration;

/**
 * Class m210909_102603_services_header
 */
class m210909_102603_services_header extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
{       $this->createTable('services_header', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
    ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('services_header');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_102603_services_header cannot be reverted.\n";

        return false;
    }
    */
}
