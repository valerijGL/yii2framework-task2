<?php

use yii\db\Migration;

/**
 * Class m210909_102633_services_cards
 */
class m210909_102633_services_cards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('services_cards', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
            'icon_class' => $this->string(100),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('services_cards');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_102633_services_cards cannot be reverted.\n";

        return false;
    }
    */
}
