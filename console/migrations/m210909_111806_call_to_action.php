<?php

use yii\db\Migration;

/**
 * Class m210909_111806_call_to_action
 */
class m210909_111806_call_to_action extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('call_to_action', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
            'button_href' => $this->string(100),
            'button_text' => $this->string(100),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('call_to_action');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_111806_call_to_action cannot be reverted.\n";

        return false;
    }
    */
}
