<?php

use yii\db\Migration;

/**
 * Class m210909_112939_our_skills_graphs
 */
class m210909_112939_our_skills_graphs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('our_skills_graphs', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'percent' => $this->integer(100)->notNull(),
            'color' => $this->string(100)->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('our_skills_graphs');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_112939_our_skills_graphs cannot be reverted.\n";

        return false;
    }
    */
}
