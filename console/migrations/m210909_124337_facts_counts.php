<?php

use yii\db\Migration;

/**
 * Class m210909_124337_facts_counts
 */
class m210909_124337_facts_counts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('facts_counts', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'title' => $this->string(35)->notNull(),
            'counts' => $this->integer(100)->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('facts_counts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_124337_facts_counts cannot be reverted.\n";

        return false;
    }
    */
}
