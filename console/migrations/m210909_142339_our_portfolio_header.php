<?php

use yii\db\Migration;

/**
 * Class m210909_142339_our_portfolio_header
 */
class m210909_142339_our_portfolio_header extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('our_portfolio_header', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('our_portfolio_header');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_142339_our_portfolio_header cannot be reverted.\n";

        return false;
    }
    */
}
