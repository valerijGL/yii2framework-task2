<?php

use yii\db\Migration;

/**
 * Class m210909_142347_our_portfolio_gallery
 */
class m210909_142347_our_portfolio_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('our_portfolio_gallery', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'title' => $this->string(100)->notNull(),
            'description' => $this->string(35),
            'image' => $this->string(100),

        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('our_portfolio_gallery');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_142347_our_portfolio_gallery cannot be reverted.\n";

        return false;
    }
    */
}
