<?php

use yii\db\Migration;

/**
 * Class m210910_104740_team_header
 */
class m210910_104740_team_header extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('team_header', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'h2' => $this->string(35)->notNull(),
            'p' => $this->string(100)->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('team_header');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210910_104740_team_header cannot be reverted.\n";

        return false;
    }
    */
}
