<?php

use yii\db\Migration;

/**
 * Class m210910_104753_team_cards
 */
class m210910_104753_team_cards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('team_cards', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean()->notNull(),
            'name' => $this->string(100)->notNull(),
            'post' => $this->string(100)->notNull(),
            'twitter_href' => $this->string(100),
            'facebook_href' => $this->string(100),
            'google_plus_href' => $this->string(100),
            'linkedin_href' => $this->string(100),
            'image' => $this->string(100),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('team_cards');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210910_104753_team_cards cannot be reverted.\n";

        return false;
    }
    */
}
