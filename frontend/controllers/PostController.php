<?php

namespace frontend\controllers;

use common\models\CallToAction;
use common\models\FactsCounts;
use common\models\FactsHeader;
use common\models\ImageGallery;
use common\models\Intro;
use common\models\FeaturedServices;
use common\models\AboutUsCard;
use common\models\AboutUsHeader;
use common\models\ContactForm;
use common\models\OurPortfolioGallery;
use common\models\OurPortfolioHeader;
use common\models\OurSkillsGraphs;
use common\models\OurSkillsHeader;
use common\models\ServicesCards;
use common\models\ServicesHeader;
use common\models\TeamCards;
use common\models\TeamHeader;
use common\widgets\Alert;
use Yii;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\Maps;

/**
 * PostController
 */
class PostController extends Controller
{

    public function actionIndex()
    {
        $about_us_card = AboutUsCard::find()->andWhere(['status' => 1])->all();
//        $about_us_card = AboutUsCard::find()->where(['status'=>1])->all();
        $about_us_header = AboutUsHeader::find()->andWhere(['status' => 1])->one();

        $intro = Intro::find()->andWhere(['status' => 1])->all();
        $featured_services = FeaturedServices::find()->andWhere(['status' => 1])->all();
        $services_header = ServicesHeader::find()->andWhere(['status'=>1])->one();
        $services_cards = ServicesCards::find()->andWhere(['status'=>1])->all();
        $call_to_action = CallToAction::find()->andWhere(['status'=>1])->one();
        $our_skills_header = OurSkillsHeader::find()->andWhere(['status'=>1])->one();
        $our_skills_graphs = OurSkillsGraphs::find()->andWhere(['status'=>1])->all();
        $facts_header = FactsHeader::find()->andWhere(['status'=>1])->one();
        $facts_counts = FactsCounts::find()->andWhere(['status'=>1])->all();
        $our_portfolio_header = OurPortfolioHeader::find()->andWhere(['status'=>1])->one();
        $our_portfolio_gallery = OurPortfolioGallery::find()->andWhere(['status'=>1])->all();
        $team_header = TeamHeader::find()->andWhere(['status'=>1])->one();
        $team_cards = TeamCards::find()->andWhere(['status'=>1])->all();


        return $this->render(
            'index',
            [
                'intro' => $intro,
                'featured_services' => $featured_services,
                'about_us_card' => $about_us_card,
                'about_us_header' => $about_us_header,
//            'about_us_card_images' => $about_us_card_images,
                'services_header' => $services_header,
                'services_cards' => $services_cards,
                'call_to_action' => $call_to_action,
                'our_skills_header' => $our_skills_header,
                'our_skills_graphs' => $our_skills_graphs,
                'facts_header' => $facts_header,
                'facts_counts' => $facts_counts,
                'our_portfolio_gallery' => $our_portfolio_gallery,
                'our_portfolio_header' => $our_portfolio_header,
                'team_cards' => $team_cards,
                'team_header' => $team_header,
            ]
        );
    }

    public function actionForm()
    {
        $model = new ContactForm();
        $maps = Maps::find()->andWhere(['status' => 1])->one();
        if ($model->load($this->request->post()) && $model->save()) {
            if ($model->validate()) {
                Yii::$app->session->setFlash('success', 'Данные отправленны');
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->file->saveAs(
                    '@frontend/web/img/files/' . $model->file->baseName . "." . $model->file->extension
                );
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
        }

        return $this->render(
            'form',
            [
                'model' => $model,
                'maps' => $maps,
            ]
        );
    }

}