<?php

use common\models\ContactForm;
use common\models\Maps;
use himiklab\yii2\recaptcha\ReCaptcha2;
use kartik\social\FacebookPlugin;
use kartik\social\TwitterPlugin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ContactForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $maps Maps */


?>

<section style="padding-top:50px;" class="section-bg wow fadeInUp">
    <div class="container">
        <div class="section-header">
            <h3>Contact Us</h3>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>

        <div class="row contact-info">
            <?= $maps->map_link ?>
        </div>

        <?php
        if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                echo Yii::$app->session->getFlash('success'); ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php
        endif; ?>
        <?php
        if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo Yii::$app->session->getFlash('error'); ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php
        endif; ?>
        <?php
        $form = ActiveForm::begin(
            [
                'options' => ['enctype' => 'multipart/form-data'],
            ]
        ) ?>
        <div class="form-group">
            <div action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name')->input('text', ['placeholder' => "Your name"])->label(false) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->Input('email', ['placeholder' => "E-mail"])->label(false) ?>
                    </div>
                </div>
                <div>
                    <?= $form->field($model, 'subject')->input('text', ['placeholder' => "Subject"])->label(false); ?>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <?= $form->field($model, 'file')->fileInput(); ?>
            </div>

            <div>
                <?= $form->field($model, 'message')->textarea(['rows' => 5, 'placeholder' => "Message"])->label(
                    false
                ); ?>
            </div>
<!--             FacebookPlugin::widget(-->
<!--                [-->
<!--                    'appId' => 'FACEBOOK_APP_ID',-->
<!--                    'type' => FacebookPlugin::SHARE,-->
<!--                    'settings' => ['size' => 'large', 'layout' => 'button_count', 'mobile_iframe' => 'false']-->
<!--                ]-->
<!--            ); -->
<!--             TwitterPlugin::widget(['type' => TwitterPlugin::SHARE, 'settings' => ['size' => 'large']]); -->
            <div class=" d-flex justify-content-center">
                <?= $form->field($model, 'reCaptcha')->widget(
                    ReCaptcha2::className(),
                    [
                        'siteKey' => '6LfAMpMbAAAAAB79nRffaqHpPN-2oZXadU5Fnhdq',
                        // unnecessary is reCaptcha component was set up
                    ]
                ) ?>
            </div>
            <div class="text-center"><?= Html::submitButton('Send Message', ['class' => 'btn btn-success']) ?></div>
            </form>
        </div>
        <?php
        ActiveForm::end(); ?>
    </div>
</section>

