<?php

use common\models\AboutUsCard;
use common\models\AboutUsHeader;
use common\models\FeaturedServices;
use common\models\ImageGallery;
use common\models\Intro;
use common\models\ServicesHeader;
use common\models\ServicesCards;
use common\models\CallToAction;
use common\models\OurSkillsHeader;
use common\models\OurSkillsGraphs;
use common\models\FactsCounts;
use common\models\FactsHeader;
use common\models\OurPortfolioGallery;
use common\models\OurPortfolioHeader;
use common\models\TeamHeader;
use common\models\TeamCards;

/* @var $this yii\web\View */
/* @var $intro Intro */
/* @var $featured_services FeaturedServices */
/* @var $about_us_card AboutUsCard */
/* @var $about_us_header AboutUsHeader */
/* @var $image_gallery ImageGallery */
/* @var $services_header ServicesHeader */
/* @var $services_cards ServicesCards */
/* @var $call_to_action CallToAction */
/* @var $our_skills_header OurSkillsHeader */
/* @var $our_skills_graphs OurSkillsGraphs */
/* @var $facts_header FactsHeader */
/* @var $facts_counts FactsCounts */
/* @var $our_portfolio_gallery OurPortfolioGallery */
/* @var $our_portfolio_header OurPortfolioHeader */
/* @var $team_header TeamHeader */
/* @var $team_cards TeamCards */

$this->title = Yii::$app->name;
?>
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <?php


                foreach ($intro as $item) :?>
                    <div class="carousel-item">
                        <div class="carousel-background"><img src="frontend/web/img/intro-carousel/<?= $item->image; ?>"
                                                              alt=""></div>
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2><?= $item->h2 ?></h2>
                                <p><?= $item->p; ?></p>
                                <a href="#featured-services"
                                   class="btn-get-started scrollto"><?= $item->btn_text; ?></a>
                            </div>
                        </div>
                    </div>
                <?php
                endforeach; ?>
                <!--                <div class="carousel-item">-->
                <!--                    <div class="carousel-background"><img src="img/intro-carousel/2.jpg" alt=""></div>-->
                <!--                    <div class="carousel-container">-->
                <!--                        <div class="carousel-content">-->
                <!--                            <h2>At vero eos et accusamus</h2>-->
                <!--                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut.</p>-->
                <!--                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!---->
                <!--                <div class="carousel-item">-->
                <!--                    <div class="carousel-background"><img src="img/intro-carousel/3.jpg" alt=""></div>-->
                <!--                    <div class="carousel-container">-->
                <!--                        <div class="carousel-content">-->
                <!--                            <h2>Temporibus autem quibusdam</h2>-->
                <!--                            <p>Beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt omnis iste natus error sit voluptatem accusantium.</p>-->
                <!--                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!---->
                <!--                <div class="carousel-item">-->
                <!--                    <div class="carousel-background"><img src="img/intro-carousel/4.jpg" alt=""></div>-->
                <!--                    <div class="carousel-container">-->
                <!--                        <div class="carousel-content">-->
                <!--                            <h2>Nam libero tempore</h2>-->
                <!--                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum.</p>-->
                <!--                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!---->
                <!--                <div class="carousel-item">-->
                <!--                    <div class="carousel-background"><img src="img/intro-carousel/5.jpg" alt=""></div>-->
                <!--                    <div class="carousel-container">-->
                <!--                        <div class="carousel-content">-->
                <!--                            <h2>Magnam aliquam quaerat</h2>-->
                <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
                <!--                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->

            </div>

            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">
                <?php
                $block_count = 0;
                foreach ($featured_services as $featured_service):
                    $block_count++;
                if($block_count != 4){
                    if ($block_count == 2) {
                        ?>
                        <div class="col-lg-4 box box-bg">
                            <i class="<?= $featured_service->icon_class ?>"></i>
                            <h4 class="title"><a href=""><?= $featured_service->h2; ?></a></h4>
                            <p class="description"><?= $featured_service->p; ?></p>
                        </div>
                        <?php
                    }else{ ?>
                        <div class="col-lg-4 box ">
                            <i class="<?= $featured_service->icon_class ?>"></i>
                            <h4 class="title"><a href=""><?= $featured_service->h2; ?></a></h4>
                            <p class="description"><?= $featured_service->p; ?></p>
                        </div>
                    <?php
                    }
                }endforeach; ?>
            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container">

            <header class="section-header">
                <h3><?= $about_us_header->h2 ?></h3>
                <p><?= $about_us_header->p ?></p>
            </header>

            <div class="row about-cols">
                <?php foreach ($about_us_card as $item):  ?>
                <div class="col-md-4 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="about-col">
                        <div class="img"><img src="frontend\web\img\<?= $item->image ?>" alt="website template image" class="img-fluid">
                            <div class="icon"><i class="<?= $item->icon_class ?>"></i></div>
                        </div>
                        <h2 class="title"><a href="<?= $item->h2_href ?>"><?= $item->h2 ?></a></h2>
                        <p><?= $item->p ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3><?= $services_header->h2 ?></h3>
                <p><?= $services_header->p ?></p>
            </header>

            <div class="row">
                <?php foreach ($services_cards as $item): ?>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="<?= $item->icon_class ?>"></i></div>
                    <h4 class="title"><a href=""><?= $item->h2 ?></a></h4>
                    <p class="description"><?= $item->p ?></p>
                </div>
                <?php endforeach; ?>

            </div>

        </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">
            <h3><?= $call_to_action->h2 ?></h3>
            <p> <?= $call_to_action->p ?></p>
            <a class="cta-btn" href="<?= $call_to_action->button_href ?>"><?= $call_to_action->button_text ?></a>
        </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Skills Section
    ============================-->
    <section id="skills">
        <div class="container">

            <header class="section-header">
                <h3><?= $our_skills_header->h2 ?></h3>
                <p><?= $our_skills_header->p ?></p>
            </header>

            <div class="skills-content">
            <?php foreach ( $our_skills_graphs as $item ): ?>
                <div class="progress">
                    <div class="progress-bar <?= $item->color ?>" role="progressbar" aria-valuenow="<?= $item->percent ?>" aria-valuemin="0"
                         aria-valuemax="100">
                        <span class="skill"><?= $item->h2 ?> <i class="val"><?= $item->percent ?>%</i></span>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>

        </div>
    </section>

    <!--==========================
      Facts Section
    ============================-->
    <section id="facts" class="wow fadeIn">
        <div class="container">

            <header class="section-header">
                <h3><?= $facts_header->h2 ?></h3>
                <p><?= $facts_header->p ?></p>
            </header>

            <div class="row counters">
            <?php foreach ($facts_counts as $item): ?>
                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up"><?= $item->counts ?></span>
                    <p><?= $item->title ?></p>
                </div>
                <?php endforeach; ?>

            </div>

            <div class="facts-img">
                <img src="frontend/web/img/facts/<?= $facts_header->image ?>" alt="" class="img-fluid">
            </div>

        </div>
    </section><!-- #facts -->

    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio" class="section-bg">
        <div class="container">
            <header style="margin-bottom: 30px" class="section-header">
                <h3 class="section-title"><?=  $our_portfolio_header->h2 ?></h3>
            </header>
<!--            <div class="row">-->
<!--                <div class="col-lg-12">-->
<!--                    <ul id="portfolio-flters">-->
<!--                        <li data-filter="*" class="filter-active">All</li>-->
<!--                        <li data-filter=".filter-app">App</li>-->
<!--                        <li data-filter=".filter-card">Card</li>-->
<!--                        <li data-filter=".filter-web">Web</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->

            <div class="row portfolio-container" style="position: relative; height: 1080px;">
                <?php foreach ($our_portfolio_gallery as $item): ?>
                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" style="position: absolute; left: 0px; top: 0px; visibility: visible; animation-name: fadeInUp;">

                    <div class="portfolio-wrap">

                        <figure>
                            <img src="frontend/web/img/our-portfolio/<?= $item->image ?>" class="img-fluid" alt="website template image">
                            <a href="frontend/web/img/our-portfolio/<?= $item->image ?>" data-lightbox="portfolio" data-title="<?= $item->title ?>" class="link-preview" title="Preview">
                                <i class="ion ion-eye"></i>
                            </a>
                        </figure>
                        <div class="portfolio-info">
                            <h4><?= $item->title ?></h4>
                            <p><?= $item->description ?></p>
                        </div>

                    </div>

                </div>
                <?php endforeach; ?>
            </div>
    </section>
    <section id="team">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3><?= $team_header->h2 ?></h3>
                <p><?= $team_header->p ?></p>
            </div>

            <div class="row">
                <?php foreach ($team_cards as $item): ?>
                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="member">
                        <img src="frontend/web/img/team/<?= $item->image ?>" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4><?= $item->name ?></h4>
                                <span><?= $item->post ?></span>
                                <div class="social">
                                    <a href="<?= $item->twitter_href ?>"><i class="fa fa-twitter"></i></a>
                                    <a href="<?= $item->facebook_href ?>"><i class="fa fa-facebook"></i></a>
                                    <a href="<?= $item->google_plus_href ?>"><i class="fa fa-google-plus"></i></a>
                                    <a href="<?= $item->linkedin_href ?>"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>

        </div>
    </section><!-- #team -->

</main>