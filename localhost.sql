-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июл 06 2021 г., 18:31
-- Версия сервера: 5.7.33
-- Версия PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `intro`
--

CREATE TABLE `intro` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `h2` varchar(35) NOT NULL,
  `p` varchar(100) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `btn_text` varchar(20) DEFAULT NULL COMMENT 'Кнопка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `intro`
--

INSERT INTO `intro` (`id`, `status`, `h2`, `p`, `image`, `btn_text`) VALUES
(1, 0, 'Привет Леня, как дела?', 'хахахахахаххахахахахаха', '1625312667_BLqpDe.jpg', 'привет'),
(3, 0, 'dsf', 'sfsdfsdf', '1625312716_Zy1ABr.png', 'ddfd'),
(4, 1, 'Нифига, это что, Максим?', 'хахахахахаха', '1625312800_YoUAht.jpg', 'Юху'),
(5, 0, 'sdkasl;dksadk;laskd;', 'PFPFPFPFPF', '1625312703_Y9qsUh.png', 'sadasjkdkasjldas');

-- --------------------------------------------------------

--
-- Структура таблицы `introslider`
--

CREATE TABLE `introslider` (
  `id` int(11) NOT NULL,
  `headline` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `introslider`
--

INSERT INTO `introslider` (`id`, `headline`) VALUES
(1, 'slider1'),
(2, 'slider2'),
(3, 'slider3'),
(4, 'slider4'),
(5, 'slider5');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1624640570),
('m130524_201442_init', 1624640573),
('m190124_110200_add_verification_token_column_to_user_table', 1624640573),
('m210627_065355_intro', 1624777193);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(3, 'admin', '1oyUPTxdT23mKRew-8N3XJXD9Hg9epJI', '$2y$13$y06/y6FTtwmm9l7oOdTgu.ZirKQ2bYpOSkMyOWigfAdQfxw2TzxVG', NULL, 'gg@mail.ru', 10, 1625592144, 1625592144),
(4, 'maxim', 'bt8LbNNXqXiv4lyv2fLGcRQudWjJaSL7', '$2y$13$m99IGFGXwtbTZqzcckT3N.llu3zuXsRYcHd2ntnkaU8/ZPMdi3sVG', NULL, 'candy@gmail.com', 10, 1625592217, 1625592217),
(5, 'jenya', 'jHaQ_KEf_6I8lu9Orin-Emc93qdKozM6', '$2y$13$lJYAI8jM7wOcw8mvg1ydEOKjB2/Ft.viWPZEeuHd97LAwoTvJfhiG', NULL, 'candy2@gmail.com', 10, 1625592257, 1625592257);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `intro`
--
ALTER TABLE `intro`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `introslider`
--
ALTER TABLE `introslider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `intro`
--
ALTER TABLE `intro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `introslider`
--
ALTER TABLE `introslider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
