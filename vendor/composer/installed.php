<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'babdbaec761484c690584cf8a8c5ee080b77b3fd',
    'name' => 'yiisoft/yii2-app-advanced',
  ),
  'versions' => 
  array (
    '2amigos/yii2-gallery-widget' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ce7de28d05a4662da7698773993b5a0245a6136',
    ),
    'abeautifulsite/simpleimage' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '54a99d302355d22ee00d9842a8dbe101a52203d8',
    ),
    'almasaeed2010/adminlte' => 
    array (
      'pretty_version' => 'v2.4.18',
      'version' => '2.4.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e7ffa67a4649dc08d2018708a38604a6c0a02ab6',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.8.0',
      'version' => '4.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2391482cd003dfdc36b679b27e9f5326bd656acd',
    ),
    'bower-asset/blueimp-gallery' => 
    array (
      'pretty_version' => 'v2.17.0',
      'version' => '2.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8336bb5934cb623608808601129db3fbf9201a09',
    ),
    'bower-asset/bootstrap' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '68b0d231a13201eb14acd3dc84e51543d16e5f7e',
    ),
    'bower-asset/inputmask' => 
    array (
      'pretty_version' => '3.3.11',
      'version' => '3.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
    ),
    'bower-asset/jquery' => 
    array (
      'pretty_version' => '3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e786e3d9707ffd9b0dd330ca135b66344dcef85a',
    ),
    'bower-asset/punycode' => 
    array (
      'pretty_version' => 'v1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'pretty_version' => '2.0.7.1',
      'version' => '2.0.7.1',
      'aliases' => 
      array (
      ),
      'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bac5e971dd391e2802dca5400bbeacbaea9eb86',
    ),
    'cebe/yii2-gravatar' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9c01bd14c9bdee9e5ae1ef1aad23f80c182c057',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.22',
      'version' => '4.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '9777ec3690ceedc4bce2ed13af7af4ca4ee3088f',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31b4b56ad53c3464fcb2c0a14d55a51a201bd3c2',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
    ),
    'codeception/module-filesystem' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '781be167fb1557bfc9b61e0a4eac60a32c534ec1',
    ),
    'codeception/module-yii2' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '09853feb15ad5e80e472c6881793a22f273a9921',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '8.1.4',
      'version' => '8.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41335f0b4dd17cf7bbc63e87943b3ae72a8bbc3',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '468dd5fe659f131fc997f5196aad87512f9b1304',
    ),
    'codeception/verify' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0bb946b6d61279f461bcc5a677ac0ed5eab9b3',
    ),
    'dektrium/yii2-user' => 
    array (
      'pretty_version' => '0.9.14',
      'version' => '0.9.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '8f2daea532f7efefc3f808e50d74a91c0de71ff4',
    ),
    'dmstr/yii2-adminlte-asset' => 
    array (
      'pretty_version' => '2.6.2',
      'version' => '2.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c96336e1960ebc6c1e72487a7c6ca1a1589519fe',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'dvizh/yii2-gallery' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'e5f6ff72dca596f64c1bf82e9871f4a187af058a',
    ),
    'dvizh/yii2-seo' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '99efdc21606233908eb9ce68a82f2bb60327fece',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c81f18a3efb941d8c4d2e025f6183b5c6d697307',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'facebook/graph-sdk' => 
    array (
      'pretty_version' => '5.7.0',
      'version' => '5.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d8250638b33d73e7a87add65f47fabf91f8ad9b',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '271d384d216e5e5c468a6b28feedf95d49f83b35',
    ),
    'fortawesome/font-awesome' => 
    array (
      'pretty_version' => 'v4.7.0',
      'version' => '4.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8386aae19e200ddb0f6845b5feeee5eb7013687',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1dc8d9cba3897165e16d12bb13d813afb1eb3fe7',
    ),
    'himiklab/yii2-recaptcha-widget' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '341ae7e19e12783f3dfd8984a49185e0a44021ea',
    ),
    'kartik-v/bootstrap-fileinput' => 
    array (
      'pretty_version' => 'v5.2.3',
      'version' => '5.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3340fd757a2a46fdeede677e366fce2e4f89d3a',
    ),
    'kartik-v/bootstrap-star-rating' => 
    array (
      'pretty_version' => 'v4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b53222d326b951d1fff0f55bd593dcb17ff941f',
    ),
    'kartik-v/dependent-dropdown' => 
    array (
      'pretty_version' => 'v1.4.9',
      'version' => '1.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '54a8806002ee21b744508a2edb95ed01d35c6cf9',
    ),
    'kartik-v/yii2-krajee-base' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbf7b58b0000f44834c18c0f2eed13a0a7d04c09',
    ),
    'kartik-v/yii2-social' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.3.x-dev',
      ),
      'reference' => '75874af84f7b6729c73e433427c9fe084e8a6801',
    ),
    'kartik-v/yii2-widget-activeform' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd54f986737d4c2b309bc72760f5aa55f78286e0e',
    ),
    'kartik-v/yii2-widget-affix' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2184119bfa518c285406156f744769b13b861712',
    ),
    'kartik-v/yii2-widget-alert' => 
    array (
      'pretty_version' => 'v1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '8be88744d2934eb0bfe701ec357fdfa8db96e3e9',
    ),
    'kartik-v/yii2-widget-colorinput' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e35e6c7615a735b65557d6c38d112b77e2628c69',
    ),
    'kartik-v/yii2-widget-datepicker' => 
    array (
      'pretty_version' => 'v1.4.7',
      'version' => '1.4.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aadeb08fe0199a62bb26940cdccafc2c169edc69',
    ),
    'kartik-v/yii2-widget-datetimepicker' => 
    array (
      'pretty_version' => 'v1.4.9',
      'version' => '1.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dadf042cf13cce84a877dff5f1e6893538f370cf',
    ),
    'kartik-v/yii2-widget-depdrop' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea347e3793fbd8273cc9bd1eb94c4b32bb55d318',
    ),
    'kartik-v/yii2-widget-fileinput' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd43bb9d9638ba117bbaa0045250645dc843fcf7f',
    ),
    'kartik-v/yii2-widget-growl' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '37e8f9f10d3bc9d71f3ef64c4aaa0e2fc83dd5dc',
    ),
    'kartik-v/yii2-widget-rangeinput' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd9019bab7e5bf570a02870d9e74387891bbdb32',
    ),
    'kartik-v/yii2-widget-rating' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '651eaa5e6a3bd19471e2a907fb17c3fd92f383e7',
    ),
    'kartik-v/yii2-widget-select2' => 
    array (
      'pretty_version' => 'v2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b3c91df7908193981033cb7ca52e83303197753',
    ),
    'kartik-v/yii2-widget-sidenav' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '87e9c815624aa966d70bb4507b3d53c158db0d43',
    ),
    'kartik-v/yii2-widget-spinner' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb10dad17a107bf14f173c99994770ca23c548a6',
    ),
    'kartik-v/yii2-widget-switchinput' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7d8ee999d79bcdc1601da5cd59439ac7eb1f5ea6',
    ),
    'kartik-v/yii2-widget-timepicker' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1dacfc3712bedf4f694d878279189e440a24b21',
    ),
    'kartik-v/yii2-widget-touchspin' => 
    array (
      'pretty_version' => 'v1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1eec4c3f3a8bf9a170e1e0682c2c89f2929d65e9',
    ),
    'kartik-v/yii2-widget-typeahead' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b7041a3cbbeb2db0a608e9f6c9b3f4f63b0069d',
    ),
    'kartik-v/yii2-widgets' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '0674838c869e5ccabc15756ee0c065ccecd09969',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.6.2',
      'version' => '3.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '06e2ebd25f2869e54a306dda991f7db58066f7f6',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc1156187f9f6c8395886fe85ed88a0a245d72e9',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be1996ed8adc35c3fd795488a653f4b518be70ea',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.15',
      'version' => '7.0.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '819f92bba8b001d4363065928088de22f25a3a48',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '28af674ff175d0768a5a978e6de83f697d4a7f05',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a853a0e183b9db7eed023d7933a858fa1c8d25a3',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.20',
      'version' => '8.5.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9deefba183198398a09b927a6ac6bc1feb0b7b70',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ae37329ee82f91efadc282cc2d527fd6065a5ef',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'rmrevin/yii2-fontawesome' => 
    array (
      'pretty_version' => '2.17.1',
      'version' => '2.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '65ce306da864f4d558348aeba040ed7876878090',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b853149eab67d4da22291d36f5b0631c0fd856e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '474fb9edb7ab891665d3bfc6317f42a0a150454b',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0150cfbc4495ed2df3872fb31b26781e4e077eb4',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'select2/select2' => 
    array (
      'pretty_version' => '4.0.13',
      'version' => '4.0.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '45f2b83ceed5231afa7b3d5b12b58ad335edd82e',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.7',
      'version' => '6.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '15f7faf8508e04471f666633addacf54c0ab5933',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '61d85c5af2fc058014c7c89504c3944e73a086f0',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b1008344647462ae6ec57559da166c2bfa5e16a',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.3.4',
      'version' => '5.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7fb120adc7f600a59027775b224c13a33530dd90',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.4.30',
      'version' => '4.4.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '4632ae3567746c7e915c33c67a2fb6ab746090c4',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce7b20d69c66a20939d8952b617506a44d102130',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '69fee1ad2332a7cbab3aca13591953da9cdb7a11',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a10000ada1e600d109a6c7632e9ac42e8bf2fb93',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '63b5bb7db83e5673936d6e3b8b3e022ff6474933',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '191afdcb5804db960d26d8566b7e9a2843cab3a0',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d224396e28d30f81969f083a58763b8b9ceb0a5',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.3.6',
      'version' => '5.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '4500fe63dc9c6ffc32d3b1cb0448c329f9c814b7',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
    ),
    'vova07/yii2-imperavi-widget' => 
    array (
      'pretty_version' => '2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4fe4a9f64a4c53d9686c80b8744e2c1366184e5',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => '2.0.43',
      'version' => '2.0.43.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f370955faa3067d9b27879aaf14b0978a805cd59',
    ),
    'yiisoft/yii2-app-advanced' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'babdbaec761484c690584cf8a8c5ee080b77b3fd',
    ),
    'yiisoft/yii2-authclient' => 
    array (
      'pretty_version' => '2.2.11',
      'version' => '2.2.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f2c547113a0592498eb0479c0d579dec11f8203',
    ),
    'yiisoft/yii2-bootstrap' => 
    array (
      'pretty_version' => '2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '83d144f4089adaa7064ad60dc4c1436daa2eb30e',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => '2.0.10',
      'version' => '2.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '94bb3f66e779e2774f8776d6e1bdeab402940510',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => '2.1.18',
      'version' => '2.1.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '45bc5d2ef4e3b0ef6f638190d42f04a77ab1df6c',
    ),
    'yiisoft/yii2-faker' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c361657143bfaea58ff7dcc9bf51f1991a46f5d',
    ),
    'yiisoft/yii2-gii' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb14e9cc4c9d80a59e1252e5200600b42aa2bfbf',
    ),
    'yiisoft/yii2-httpclient' => 
    array (
      'pretty_version' => '2.0.14',
      'version' => '2.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '50d670d2e1a30a354c27aeebf806a2db16954836',
    ),
    'yiisoft/yii2-swiftmailer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '09659a55959f9e64b8178d842b64a9ffae42b994',
    ),
    'yurkinx/yii2-image' => 
    array (
      'pretty_version' => 'v1.3',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '590795d0e94c909c636cba1007a619012e118ac9',
    ),
  ),
);
